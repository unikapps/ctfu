<nav class="nav flex-column">
    <a class="nav-link" href="{{ route('tasks.index') }}">Tasks list</a>
    <a class="nav-link" href="{{ route('tasks.create') }}">Add new task</a>
</nav>