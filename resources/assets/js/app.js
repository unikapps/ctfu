/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');


/**
 * Reorder task
 */
require('sortablejs');

let el = document.getElementById('tasks');

let options = {
    onEnd: function (event) {
        //When the sorting ended I select the tasks and add them in array
        // the order will be the array order
        let tasks = $('#tasks').children('.list-group-item');
        let ids = [];
        _.forEach(tasks, function (task) {
            ids.push($(task).data('id'))
        });

        // Send a request to the server to update the order
        axios.post('/api/reorder-tasks', {tasks: ids})
            .then((response) => {

            })
            .catch((reason) => {

            })
    },
}

let sortable = new Sortable(el, options);

/**
 * Filter by project
 */
let form = $("#filter-form");
form.find('select').on('change', function () {
    form.submit();
});