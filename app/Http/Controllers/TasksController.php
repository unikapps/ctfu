<?php

namespace App\Http\Controllers;

use App\Http\Requests\TaskStoreRequest;
use App\Project;
use App\Task;
use Illuminate\Http\Request;

class TasksController extends Controller
{
    /**
     * Display the list of Tasks
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $tasks = Task::orderBy('priority');

        if ($request->project){
            $tasks= $tasks->where('project_id', $request->project);
        }

        $tasks= $tasks->paginate(15);

        $projects = Project::all();
        return view('tasks.index', compact('tasks', 'projects'));
    }

    /**
     * Display the form to create new Task with the projects list
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $projects = Project::all();

        return view('tasks.create', compact('projects'));
    }

    /**
     * Store submitted Task after validating the form data
     *
     * @param TaskStoreRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(TaskStoreRequest $request)
    {
        $task             = new Task;
        $task->name       = $request->name;
        $task->priority   = $request->priority;
        $task->project_id = $request->project_id;

        if ($task->save()) {
            return back();
        }
    }

    /**
     * Display the page to edit the task
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $task     = Task::with('project')->where('id', $id)->first();
        $projects = Project::all();

        return view('tasks.edit', compact('projects', 'task'));
    }

    /**
     * Update the task
     *
     * @param TaskStoreRequest $request
     * @param Task $task
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(TaskStoreRequest $request, Task $task)
    {
        $taskData = [
            'name'       => $request->name,
            'project_id' => $request->project_id,
            'priority'   => $request->priority,
        ];

        $task->update($taskData);

        return redirect()->route('tasks.index');

    }

    /**
     * Destroy the task and go back to tasks list
     *
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        Task::find($id)->delete();

        return back();
    }

    /**
     * Re-order tasks
     * @todo: enqueue this operation if the list of tasks  is big
     *
     * @param Request $request
     *
     * @return array
     */
    public function reOrder(Request $request)
    {
        foreach ($request->tasks as $priority => $id) {
            Task::find($id)->update(['priority' => $priority + 1]);
        }

        return response()->json([
            'data' => [
                'status' => 'successfully reordered the tasks',
            ],
        ], 200);
    }
}
