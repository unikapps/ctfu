#Installation instructions
 
 ## 2. Test zipped  application
* unzip the application
 
* Navigate to the directory from command line and run
  
```shell
php -S localhost:8080 -t public
```
  
## 1.Installation from the repository

**Clone the repository**

 ```shell
 git clone https://unikapps@bitbucket.org/unikapps/ctt2.git 
 ```
 
**Install dependencies:**
```
composer install
```

**Copy .env.example to .env** 

 ```shell
 cp .env.example .env 
 ```
 **Generate the key**
 
 ````
 php artisan key:generate
 ````

**Edit your database configuration:** 
In my case i used sqlite, if you would like to do the same, please change 
DB_CONNECTION in .env to sqlite and create a file named database.sqlite in database folder

 ```shell
 touch database/database.sqlite
 ```
 
**Run migrations**
 
 ```shell
 php artisan migrate
 ```
 
**For simplification I created a seeder for projects to have some dummy data, please run it first:**
 ```shell
 php artisan db:seed
 ```
 
 **And finally run the server:**
 ```shell
 php -S localhost:8080 -t public
 ```

  
 ## Credits
 For the sorting i used this js library
 https://github.com/RubaXa/Sortable
 
 